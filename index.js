import React, { Component, createContext } from 'react'
import PropTypes from 'prop-types'
import { withIpfs } from '../ipfs'

const ClipboardContext = createContext()

class Clipboard extends Component {
  static propTypes = {
    localKey: PropTypes.string.isRequired
  }

  state = {
    clip: []
  }

  constructor( props ) {
    super( props )
    this.put = this.put.bind( this )
    this.remove = this.remove.bind( this )
    this.clean = this.clean.bind( this )
  }

  async componentDidMount() {
    this.loadClip()
  }

  loadClip() {
    const { localKey } = this.props
    const clipAddress = window.localStorage.getItem( localKey )

    if ( !clipAddress )
      return this.save()

    this.readClip( clipAddress )
  }

  async readClip( address ) {
    const { node: api } = this.props.withIpfs

    const clip = await api.dag.get( address )
    this.setState( { clip: clip.value } )
  }

  async save( clip = [] ) {
    const { node: api } = this.props.withIpfs
    const { localKey } = this.props

    this.setState( { clip } )

    const cid = await api.dag.put( clip )
    window.localStorage.setItem( localKey, cid.toString() )
  }

  put( item ) {
    const { clip } = this.state
    clip.push( item )
    this.save( clip )
  }

  remove( index ) {
    const { clip } = this.state
    clip.splice( index, 1 )
    this.save( clip )
  }

  clean() {
    this.save()
  }

  render() {
    const { children } = this.props
    const { clip } = this.state
    const { put, remove, clean } = this

    return (
      <ClipboardContext.Provider value={ {
        clip,
        put,
        remove,
        clean
      } }>
        { children }
      </ClipboardContext.Provider>
    )
  }
}

const withClipboard = ( ComponentAlias ) => {
  return props => (
    <ClipboardContext.Consumer>
      { context => (
        <ComponentAlias { ...props } withClipboard={ context } />
      ) }
    </ClipboardContext.Consumer>
  )
}

export default withIpfs( Clipboard )

export {
  ClipboardContext,
  withClipboard
}
